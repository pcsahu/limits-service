package com.microservices.limitsservice.bean;

public class LimitConfigs {

	private int maximun;
	
	private int minimum;

	public LimitConfigs(int maximun, int minimum) {
		super();
		this.maximun = maximun;
		this.minimum = minimum;
	}

	public LimitConfigs() {
		super();
	}

	public int getMaximun() {
		return maximun;
	}

	public void setMaximun(int maximun) {
		this.maximun = maximun;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMinimum(int minimum) {
		this.minimum = minimum;
	}	
}
