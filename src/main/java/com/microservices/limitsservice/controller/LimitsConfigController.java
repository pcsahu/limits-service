package com.microservices.limitsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.limitsservice.bean.LimitConfigs;
import com.microservices.limitsservice.config.Configs;

@RestController
public class LimitsConfigController {

	@Autowired
	private Configs config;
	
	@GetMapping("/limits")
	public LimitConfigs retriveLimitsFromConfigs() {
		return new LimitConfigs(config.getMaximum(), config.getMinimum());
	}
}
