package com.microservices.limitsservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("limits-service")
public class Configs {

	private int maximum;

	private int minimum;

	public int getMaximum() {
		return maximum;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMaximum(int maximun) {
		this.maximum = maximun;
	}

	public void setMinimum(int minimum) {
		this.minimum = minimum;
	}
}
